/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  pathPrefix: `/gatsby-demo`,
  siteMetadata: {
    title: "Goo",
  },
  /* Your site config here */
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `posts`,
        path: `${__dirname}/src/pages/posts/`,
      },
    },
    {
      resolve: `gatsby-plugin-postcss`,
      options: {
        postCssPlugins: [require(`postcss-preset-env`)({ stage: 0 })],
      },
    },
    {
      resolve: `gatsby-mdx`,
      options: {
        defaultLayouts: {
          default: require.resolve("./src/layout/default.tsx"),
          posts: require.resolve("./src/layout/posts.tsx"),
          // default: require.resolve("./src/components/default-page-layout.js")
        },
      },
    },
    "gatsby-plugin-typescript",
    "gatsby-plugin-tslint",
  ],
}
