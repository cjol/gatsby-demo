declare module "*.pcss" {
  const exports: { [exportName: string]: string }
  export default exports
}

declare module "*.css" {
  const exports: { [exportName: string]: string }
  export = exports
}
