import { IDefaultProps } from "../types"

export default (props: IDefaultProps) => {
  return props.children
}
