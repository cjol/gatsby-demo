import React from "react"
import styles from "./Counter.module.css"

export const Counter = (props: { init?: number }) => {
  const [count, set] = React.useState(props.init || 0)
  const inc = () => set(c => c + 1)
  const dec = () => set(c => c - 1)

  const [title, setTitle] = React.useState("Type here")

  return (
    <div>
      <h1>{title}</h1>
      <h2>{count}</h2>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <button className={"foo " + styles.button} onClick={inc}>
          +
        </button>
        <button className={styles.button} onClick={dec}>
          -
        </button>
      </div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        Change title:{" "}
        <input value={title} onChange={e => setTitle(e.target.value)} />
      </div>
    </div>
  )
}
