import * as styles from "./NavBar.css"
import { Link } from "gatsby"
import * as React from "react"

export interface NavBarItem {
  title: string
  link: string
}

const useLeftMeasure = (deps?: readonly any[]) => {
  const itemRef = React.useRef<HTMLAnchorElement>()
  const parentRef = React.useRef<HTMLDivElement>()
  const [pos, setPos] = React.useState(0)
  React.useLayoutEffect(() => {
    if (itemRef.current && parentRef.current)
      setPos(
        itemRef.current.getBoundingClientRect().width / 2 +
          itemRef.current.getBoundingClientRect().left -
          parentRef.current.getBoundingClientRect().left
      )
  }, deps)
  return [pos, itemRef, parentRef] as [
    number,
    React.MutableRefObject<any>,
    React.MutableRefObject<any>
  ]
}

export const NavBar = (props: { pages: NavBarItem[] }) => {
  const [active, setActive] = React.useState<string | null>()
  const activator = (link: string) => () => setActive(link)
  // only deselect if we're still activating
  const deactivator = (link: string) => () =>
    setActive(prev => (prev === link ? null : prev))
  const refTarget = active || "/"
  const [left, activeRef, parentRef] = useLeftMeasure([active])

  return (
    <div className={styles.navbar}>
      <div className={styles.brand}></div>
      <div className={styles.links} ref={parentRef}>
        <div
          className={[
            styles.underline,
            active === null ? styles.current : "",
          ].join(" ")}
          style={{ left }}
        />
        {/* {!active ? <div {...morph} className=""></div> : null} */}
        {props.pages.map(x => (
          <div className={styles.link}>
            <a
              href={x.link}
              key={x.link}
              ref={x.link === refTarget ? activeRef : undefined}
              className={styles.link}
              onMouseEnter={activator(x.link)}
              onMouseLeave={deactivator(x.link)}
            >
              {x.title}
            </a>
          </div>
        ))}
        <Link to="/search">Search</Link>
        <Link to="/menu">Menu</Link>
      </div>
    </div>
  )
}
