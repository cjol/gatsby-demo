import * as React from "react"
import { storiesOf } from "@storybook/react"
// import { action } from "@storybook/addon-actions"
import { NavBar } from "."

const stories = storiesOf("Components", module)

stories.add(
  "NavBar",
  () => (
    <NavBar
      pages={[
        { link: "/", title: "Foo" },
        { link: "/bar", title: "Bar" },
        { link: "/lark", title: "Lark" },
        { link: "/lark2", title: "Lark" },
        { link: "/lark3", title: "Lark" },
      ]}
    />
  ),
  { info: { inline: true } }
)
