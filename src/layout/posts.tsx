import React from "react"
import { IDefaultProps } from "../types"

export default ({ children, pageContext }: IDefaultProps) => {
  return (
    <div>
      <h1>Blog Post: {pageContext.frontmatter.title}</h1>
      {children}
    </div>
  )
}
