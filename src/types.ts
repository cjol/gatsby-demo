export interface IDefaultProps {
  pageContext: { frontmatter: { [k: string]: any } }
  children: React.ReactChildren
}
